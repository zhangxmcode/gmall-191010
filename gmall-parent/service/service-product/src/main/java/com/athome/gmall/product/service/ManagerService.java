package com.athome.gmall.product.service;

import com.athome.gmall.model.product.BaseCategory1;

import java.util.List;

/**
 * @author zhangxm
 * @create 2020-04-02 11:54
 */

public interface ManagerService {
    // 获取一级分类
    List<BaseCategory1> getCategory1();
}
