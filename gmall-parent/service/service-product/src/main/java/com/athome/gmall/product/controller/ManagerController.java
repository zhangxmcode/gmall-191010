package com.athome.gmall.product.controller;

import com.athome.gmall.common.result.Result;
import com.athome.gmall.model.product.BaseCategory1;
import com.athome.gmall.product.service.ManagerService;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author zhangxm
 * @create 2020-04-02 11:50
 */
@ApiModel("后台管理")
@RestController
@RequestMapping("/admin/product")
public class ManagerController {

    @Autowired
    private ManagerService managerService;

    //获取一级分类
    @ApiOperation("查询所有一级分类")
    @GetMapping("/getCategory1")
    public Result getCategory1() {
        List<BaseCategory1> baseCategory1List = managerService.getCategory1();
        return Result.ok(baseCategory1List);
    }

}
