package com.athome.gmall.product.service.impl;

import com.athome.gmall.model.product.BaseCategory1;
import com.athome.gmall.product.mapper.BaseCategory1Mapper;
import com.athome.gmall.product.service.ManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zhangxm
 * @create 2020-04-02 11:55
 */
@Service
public class ManagerServiceImpl implements ManagerService{

    @Autowired
    private BaseCategory1Mapper baseCategory1Mapper;

    // 获取一级分类
    @Override
    public List<BaseCategory1> getCategory1() {
        return baseCategory1Mapper.selectList(null);
    }
}
