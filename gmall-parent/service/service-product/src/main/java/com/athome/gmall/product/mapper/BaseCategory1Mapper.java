package com.athome.gmall.product.mapper;

import com.athome.gmall.model.product.BaseCategory1;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.mapstruct.Mapper;

/**
 * @author zhangxm
 * @create 2020-04-02 11:57
 */
@Mapper
public interface BaseCategory1Mapper extends BaseMapper<BaseCategory1> {

}
